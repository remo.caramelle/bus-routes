import { getTrips, getStopInfo } from '../api-client/metropilis-fe-test/trips';
import { TripAdapterSingleton, ITrip } from '../domain/models/trip';
import { StopAdapterSingleton } from '../domain/models/stop'

export const getTripsServices = () => {
  return getTrips().then(response => {
    return response.map((trip: ITrip) => {
      return TripAdapterSingleton.adapt(trip);
    })
  }
  );
}

export const getStopServices = (stopId: number) => {
  return getStopInfo(stopId).then(response => {
    response.id = stopId;
    return StopAdapterSingleton.adapt(response);
  })
}

