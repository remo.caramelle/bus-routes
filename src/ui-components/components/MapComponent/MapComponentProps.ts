import { ITrip } from "../../../domain/models/trip";

export interface MapComponentProps {
  selectedTrip: ITrip
}

export interface PointProps {
    _latitude: number;
    _longitude: number;
}
