export const getLatLngBoundsClass = () => {
  if(window.google) return new window.google.maps.LatLngBounds();
  return null;
};

export const boundMap = (map, bounds) => {
  map.fitBounds(bounds);
};

