import React, { useState, useEffect, Fragment } from 'react';
import { GoogleMap, Marker, Polyline, InfoWindow, } from "@react-google-maps/api"
import { MapComponentProps, PointProps } from './MapComponentProps'
import { getLatLngBoundsClass, boundMap } from './Maputils.js';
import { getStopServices } from '../../../services/trips';
import markerIconStart from '../../assets/icons/circle-filled-start.png';
import markerIconEnd from '../../assets/icons/circle-filled-end.png';
import markerIconBlue from '../../assets/icons/circle-filled-blue.png';

const MapComponent = ({ selectedTrip }: MapComponentProps) => {
  const [center, ] = useState({
    lat: 41.3818,
    lng: 2.1685
  });
  const [polyline, setPolyline] = useState<{ lat: number; lng: number; }[]>([]);
  const [mapInstance, setMapInstance] = useState();
  const [infoOpen, setInfoOpen] = useState(false);
  const [infoPos, setInfoPos] = useState<PointProps>();
  const [popupMessage, setPopupMessage] = useState<string>();

  const onMapLoad = (map: any) => {
    setMapInstance(map);
  }

  const getPosition = (point: PointProps) => {
    return { lat: point._latitude, lng: point._longitude }
  }

  useEffect(() => {
    if (selectedTrip) {
      setInfoOpen(false);
      const bounds = getLatLngBoundsClass();
      let route = [];
      route.push(getPosition(selectedTrip.origin.point));
      bounds.extend(getPosition(selectedTrip.origin.point));
      let stopIds: number[] = []
      if (selectedTrip.stops) selectedTrip.stops.forEach(trip => {
        if (trip.point) {
          route.push(getPosition(trip.point));
          bounds.extend(getPosition(trip.point));
          stopIds.push(trip.id);
        }
      })
      route.push(getPosition(selectedTrip.destination.point));
      bounds.extend(getPosition(selectedTrip.destination.point));
      setPolyline(route);
      boundMap(mapInstance, bounds);
    }
  }, [mapInstance, selectedTrip]);

  const handleStopClick = (event: any, type: string, id: number | null) => {
    setPopupMessage("");
    if (id) {
      getStopServices(id).then(response => {
        setPopupMessage(response.address);
        setInfoOpen(true);
      })
    }
    else {
      if (type === 'origin') {
        setPopupMessage(selectedTrip.origin.address);
      } else {
        setPopupMessage(selectedTrip.destination.address);
      }
      setInfoOpen(true);
    }
    setInfoPos(event.latLng);
  }

  return (
    <GoogleMap
      id='example-map'
      zoom={13}
      center={center}
      onLoad={onMapLoad}
      mapContainerStyle={{
        height: '100%',
        width: '100%',
      }}
    >
      {
        selectedTrip &&
        <Fragment>
          <Marker
            position={getPosition(selectedTrip.origin.point)}
            icon={markerIconStart}
            onClick={event => handleStopClick(event, "origin", null)}
          />
          {
            selectedTrip.stops &&
            selectedTrip.stops.map(stop => (
              stop.point &&
              <Marker
                key={stop.id}
                position={getPosition(stop.point)}
                icon={markerIconBlue}
                onClick={event => handleStopClick(event, "stop", stop.id)}
              />
            ))
          }
          <Marker
            position={getPosition(selectedTrip.destination.point)}
            icon={markerIconEnd}
            onClick={event => handleStopClick(event, "destination", null)}
          />
        </Fragment>
      }
      {infoOpen && infoPos && (
        <InfoWindow
          position={infoPos}
          onCloseClick={() => setInfoOpen(false)}
        >
          <div>
            {<h4>{popupMessage}</h4>}
          </div>
        </InfoWindow>
      )}
      <Polyline 
        path={polyline} 
        options={
          {strokeColor: '#1d1b3b'}
        }
      />
    </GoogleMap>
  );
};

export default MapComponent;
