import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { MenuComponentProps } from './MenuComponentProps';
import TripCard from '../TripCard/TripCard';

const MenuComponent = ({ trips, selectedTripPosition, handleTripClick}: MenuComponentProps) => {
  return (
    <Row className="menu-component">
      {trips.map((trip, index) =>
        (<Col 
          className={`
            trip-container 
            ${index === 0 ? 'first-trip' : index === trips.length - 1 ? 'last-trip' : ''} 
            ${selectedTripPosition === index ? 'selected-trip' : ''}
            `}
          sm={12} key={index}
          onClick={() => handleTripClick(index)}
          >
          <TripCard trip={trip} />
        </Col>)
      )}
    </Row>
  );
};

export default MenuComponent;