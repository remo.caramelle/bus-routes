import { ITrip } from "../../../domain/models/trip";

export const tripsMock: ITrip[] = 
    [
      {
          "status": "ongoing",
          "origin": {
              "address": "Metropolis:lab, Barcelona",
              "point": {
                  "_latitude": 41.38074,
                  "_longitude": 2.18594
              }
          },
          "stops": [
              {
                  "point": {
                      "_latitude": 41.37653,
                      "_longitude": 2.17924
                  },
                  "id": 1
              },
              {
                  "point": {
                      "_latitude": 41.35179,
                      "_longitude": 2.15494
                  },
                  "id": 2
              },
              {
                  "point": {
                      "_latitude": 41.43853,
                      "_longitude": 2.00094
                  },
                  "id": 3
              },
              {
                  "point": {
                      "_latitude": 41.47711,
                      "_longitude": 1.91843
                  },
                  "id": 4
              }
          ],
          "destination": {
              "address": "Seat HQ, Martorell",
              "point": {
                  "_latitude": 41.49958,
                  "_longitude": 1.90307
              }
          },
          "endTime": "2018-12-18T09:00:00.000Z",
          "startTime": "2018-12-18T08:00:00.000Z",
          "description": "Barcelona a Martorell",
          "driverName": "Alberto Morales",
          "route": "sdq{Fc}iLj@zR|W~TryCzvC??do@jkKeiDxjIccLhiFqiE`uJqe@rlCy~B`t@sK|i@"
      },
      {
          "origin": {
              "point": {
                  "_latitude": 41.38074,
                  "_longitude": 2.18594
              },
              "address": "Metropolis:lab, Barcelona"
          },
          "stops": [
              {
                  "point": {
                      "_latitude": 41.39869,
                      "_longitude": 2.1692
                  },
                  "id": 5
              },
              {
                  "point": {
                      "_latitude": 41.42522,
                      "_longitude": 2.09084
                  },
                  "id": 6
              }
          ],
          "destination": {
              "address": "Sant Cugat Centre, El Valles",
              "point": {
                  "_latitude": 41.46693,
                  "_longitude": 2.08006
              }
          },
          "endTime": "2018-12-18T08:45:00.000Z",
          "startTime": "2018-12-18T08:00:00.000Z",
          "description": "Barcelona a Sant cugat",
          "driverName": "Joaquin Sabina",
          "route": "sdq{Fc}iLeoBrgBs@xsGecD|sEswEv|Cek@cyA",
          "status": "ongoing"
      }
  ];
