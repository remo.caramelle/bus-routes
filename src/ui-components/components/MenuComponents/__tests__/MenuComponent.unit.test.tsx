import React from 'react';
import MenuComponent from '../MenuComponent';
import { tripsMock } from '../MenuComponent.mock';
import Enzyme, { mount, ReactWrapper } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() })

const tripClick = jest.fn();
let MenuComp: ReactWrapper;

beforeEach(() => {
  MenuComp = mount(
    <MenuComponent 
      trips={tripsMock}
      selectedTripPosition={0}
      handleTripClick={tripClick}
    />
  );
});

afterEach(() => {
  MenuComp.unmount();
});

describe("Menu Component unit tests", () => {
  it("Call on click", () => {
    MenuComp.find('.trip-container').at(0).simulate('click');   
    expect(tripClick).toBeCalled();;
  });
}

)
