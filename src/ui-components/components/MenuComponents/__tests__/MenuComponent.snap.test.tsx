import React from 'react';
import renderer from 'react-test-renderer';
import MenuComponent from '../MenuComponent';
import { tripsMock } from '../MenuComponent.mock';

const tripClick = jest.fn();

test('FirstComponent Snapshot', () => {
  const component = renderer.create(
    <MenuComponent 
      trips={tripsMock}
      selectedTripPosition={0}
      handleTripClick={tripClick}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});