import { ITrip } from '../../../domain/models/trip';

export interface MenuComponentProps {
  trips: ITrip[];
  selectedTripPosition: number;
  handleTripClick: (pos: number) => void;
}