import React from 'react';
import renderer from 'react-test-renderer';
import TripCard from '../TripCard';
import { tripMock } from '../TripCard.mock';

const tripClick = jest.fn();

test('FirstComponent Snapshot', () => {
  const component = renderer.create(
    <TripCard 
      trip={tripMock}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});