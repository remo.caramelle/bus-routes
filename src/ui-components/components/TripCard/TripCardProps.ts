import { ITrip } from '../../../domain/models/trip';

export interface TripCardProps {
  trip: ITrip;
}