import React from 'react';
import { TripCardProps } from './TripCardProps';
import moment from 'moment';

const TripCard = ({trip}: TripCardProps) => {
  const ms = moment(trip.endTime).diff(moment(trip.startTime));
  const duration = moment.duration(ms);
  var formatedDuration = Math.floor(duration.asHours()) + moment.utc(ms).format(":mm:ss");
  
  return (
    <div className="trip-card">
      <p className="trip-description">{trip.description}</p>
      <p><span>from:</span> {trip.origin.address}</p>
      <p><span>to:</span> {trip.destination.address}</p>
      <p><span>driver name:</span>  {trip.driverName}</p>
      <p><span>duration:</span> {formatedDuration}</p>
    </div>
  );
};

export default TripCard;