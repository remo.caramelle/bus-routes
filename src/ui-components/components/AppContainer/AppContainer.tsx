import React, { useState, useEffect, Fragment } from 'react';
import { ITrip } from '../../../domain/models/trip';
import { getTripsServices } from '../../../services/trips';
import { Spinner } from 'react-bootstrap';
import '../../styles/main.module.scss';
import MenuComponent from '../MenuComponents/MenuComponent';
import MapComponent from '../MapComponent/MapComponent';
import { LoadScriptNext } from '@react-google-maps/api';

const AppContainer = () => {
  const [trips, setTrips] = useState<ITrip[]>([]);
  const [selectedTripPosition, setSelectedTripPosition] = useState<number>(-1);

  useEffect(() => {
    getTripsServices().then(response => {
      setTrips(response);
    });
  }, []);

  const handleTripClick = (pos: number) => {
    setSelectedTripPosition(pos);
  }

  return (
    <div className="app-container">
      {trips.length ? (
        <Fragment>
          <div className="menu-container">
            <MenuComponent
              trips={trips}
              selectedTripPosition={selectedTripPosition}
              handleTripClick={handleTripClick}
            />
          </div>
          <LoadScriptNext
            id="script-loader"
            googleMapsApiKey="AIzaSyD1aCwKJ42a5xoT7lk4EEgdHueW0vMY8TA"
          >
            <div className="map-container">
              <MapComponent
                selectedTrip={trips[selectedTripPosition]}
              />
            </div>
          </LoadScriptNext>
        </Fragment>
      ) :
        <div className="spinner-container">
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>}
    </div>
  );
};

export default AppContainer;