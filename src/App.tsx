import React from 'react';
import './App.scss';
import AppContainer from './ui-components/components/AppContainer/AppContainer';

function App() {
  return (
      <AppContainer />
  );
}

export default App;
