import axios, { AxiosRequestConfig } from 'axios';

export const REQUEST_RETRY_LIMIT = 3;

export let requestRetry = 0;
export function setRequestRetry(retries: number) {
  requestRetry = retries;
}

/**
 * Makes a GET request
 * @param url The URL to make the request
 * @param config An `AxiosRequestConfig` object with specific settings for the request.
 */
export function get(url: string, config?: AxiosRequestConfig): Promise<any> {
  // NOTE: Use this method to implement headers configuration, security and other stuff related with GET method requests.
  return axios
    .get(url, { ...config })
    .then(response => {
      setRequestRetry(0);
      return response;
    })
    .catch(err => {
      throw err;
    });
}