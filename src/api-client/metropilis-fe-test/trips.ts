import {get} from '../core/api-base';

const url = 'https://europe-west1-metropolis-fe-test.cloudfunctions.net/api';

export async function getTrips(): Promise<any> {
  const response = await get(`${url}/trips`);
  return response.data;
}

export async function getStopInfo(id: number): Promise<any> {
  const response = await get(`${url}/stops/${id}`);
  return response.data;
}
