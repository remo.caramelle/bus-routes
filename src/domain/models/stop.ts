import Adapter from '../adapter';

export interface IStop {
  readonly id: number;
  readonly userName: string;
  readonly point: {
    readonly _latitude: number;
    readonly _longitude: number;
  }
  readonly price: number;
  readonly stopTime: string;
  readonly paid: boolean;
  readonly address: string;
  readonly tripId: number;
}


export class Stop {
  readonly id: number;
  readonly userName: string;
  readonly point: {
    readonly _latitude: number;
    readonly _longitude: number;
  }
  readonly price: number;
  readonly stopTime: string;
  readonly paid: boolean;
  readonly address: string;
  readonly tripId: number;

  constructor(stop: IStop) {
    this.id = stop.id;
    this.userName = stop.userName;
    this.point = stop.point;
    this.price = stop.price;
    this.stopTime = stop.stopTime;
    this.paid = stop.paid;
    this.address = stop.address;
    this.tripId = stop.tripId;
  }
}

export class StopAdapter implements Adapter<Stop>{
  adapt(item: IStop) {
    return new Stop(item);
  }
}

export const StopAdapterSingleton: StopAdapter = new StopAdapter();