import Adapter from '../adapter';

export interface ITrip {
  readonly status: string;
  readonly destination: {
    readonly address: string;
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    }
  };
  readonly endTime: string;
  readonly startTime: string;
  readonly description: string;
  readonly driverName: string;
  readonly origin: {
    readonly address: string;
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    };
  };
  readonly stops: {
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    },
    readonly id: number;
  }[];
  readonly route: string;
}


export class Trip {
  readonly status: string;
  readonly destination: {
    readonly address: string;
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    };
  }
  readonly endTime: string;
  readonly startTime: string;
  readonly description: string;
  readonly driverName: string;
  readonly origin: {
    readonly address: string;
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    };
  };
  readonly stops: {
    readonly point: {
      readonly _latitude: number;
      readonly _longitude: number;
    },
    readonly id: number;
  }[];

  constructor(trip: ITrip) {
    this.status = trip.status;
    this.destination = trip.destination;
    this.endTime = trip.endTime;
    this.startTime = trip.startTime;
    this.endTime = trip.endTime;
    this.description = trip.description;
    this.driverName = trip.driverName;
    this.status = trip.status;
    this.origin = trip.origin;
    this.stops = trip.stops;
  }
}

export class TripAdapter implements Adapter<Trip>{
  adapt(item: ITrip) {
    return new Trip(item);
  }
}

export const TripAdapterSingleton: TripAdapter = new TripAdapter();